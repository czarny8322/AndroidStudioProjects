﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : ApiController
    {
        [HttpGet]
        public JsonResult<List<ParkingModel>> getAll()
        {
            var db = new ParkingDbContext().MyParkingModel.ToList();
            if (db != null)
            {
                return Json(db);
            }
            else return null;
            
        }

        [HttpGet]
        public JsonResult<ParkingModel> Value(int id)
        {
            var db = new ParkingDbContext().MyParkingModel.Where(x => x.ID == id).FirstOrDefault();
            return Json(db);
           
        }
        [HttpPost]
        
        public IHttpActionResult SaveNewValue([FromBody]ParkingModel model)
        {
            //string json = JsonConvert.SerializeObject(model, Formatting.Indented);
            //var httpContent = new StringContent(json);
            
            using(ParkingDbContext db = new ParkingDbContext())
            {
                var park_model = new ParkingModel()
                {
                    PlaceNumber = model.PlaceNumber,
                    PlaceStatus = model.PlaceStatus,
                    Color = model.Color
                };
                db.MyParkingModel.Add(park_model);
                db.SaveChanges();

                return Ok(db.MyParkingModel.ToList());
            }

        }
        
        [HttpPut]
        public IHttpActionResult UpdateValue([FromBody]ParkingModel model)
        {
            using (ParkingDbContext db = new ParkingDbContext())
            {
                var myEntity = db.MyParkingModel.Where(x => x.ID == model.ID).FirstOrDefault();
                if (myEntity != null)
                {
                    myEntity.PlaceNumber = model.PlaceNumber;
                    myEntity.PlaceStatus = model.PlaceStatus;
                    myEntity.Color = model.Color;
                    db.SaveChanges();
                } 

                return Ok(db.MyParkingModel.ToList());
            }
        }

        [HttpDelete]
        public void RemoveValue(int id)
        {
        }
    }
}
