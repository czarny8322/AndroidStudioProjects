package com.sourcey.materiallogindemo.Retrofit;

import com.google.gson.annotations.SerializedName;

public class ApiModel {
    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getPlaceNumber() {
        return PlaceNumber;
    }

    public void setPlaceNumber(String placeNumber) {
        PlaceNumber = placeNumber;
    }

    public String getPlaceStatus() {
        return PlaceStatus;
    }

    public void setPlaceStatus(String placeStatus) {
        PlaceStatus = placeStatus;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    @SerializedName("ID")
            private Integer ID;

    @SerializedName("PlaceNumber")
            private String PlaceNumber;

    @SerializedName("PlaceStatus")
            private String PlaceStatus;

    @SerializedName("Color")
            private String Color;
}
