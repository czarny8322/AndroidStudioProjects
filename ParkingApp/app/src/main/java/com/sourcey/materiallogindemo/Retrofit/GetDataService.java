package com.sourcey.materiallogindemo.Retrofit;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface GetDataService {
    @GET("/api/home/")
    Call<List<ApiModel>> getAll();


    @POST("/api/home/")
    @FormUrlEncoded
    Call<List<ApiModel>> saveItem(@Field("PlaceNumber") String PlaceNumber,
                        @Field("PlaceStatus") String PlaceStatus,
                        @Field("Color") String Color);

}
