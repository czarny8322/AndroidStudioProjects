package com.sourcey.materiallogindemo;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class MyAdapter extends RecyclerView.Adapter {
    // źródło danych
    private List<CarPlace> mArticles = new ArrayList<>();


    // obiekt listy artykułów
    private RecyclerView mRecyclerView;

    // implementacja wzorca ViewHolder
    // każdy obiekt tej klasy przechowuje odniesienie do layoutu elementu listy
    // dzięki temu wywołujemy findViewById() tylko raz dla każdego elementu

    private class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView mPlaceNumber;
        public TextView mStatus;

        public MyViewHolder(View pItem) {
            super(pItem);
            mPlaceNumber = (TextView) pItem.findViewById(R.id.article_title);
            mStatus = (TextView) pItem.findViewById(R.id.article_subtitle);
        }
    }

    // konstruktor adaptera
    public MyAdapter(List<CarPlace> pArticles, RecyclerView pRecyclerView){
        mArticles = pArticles;
        mRecyclerView = pRecyclerView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        // tworzymy layout artykułu oraz obiekt ViewHoldera
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.parking_space_layout, viewGroup, false);

        // dla elementu listy ustawiamy obiekt OnClickListener,
        // który usunie element z listy po kliknięciu na niego
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // odnajdujemy indeks klikniętego elementu
//                int positionToDelete = mRecyclerView.getChildAdapterPosition(v);
//                // usuwamy element ze źródła danych
//                mArticles.remove(positionToDelete);
//                // poniższa metoda w animowany sposób usunie element z listy
//                notifyItemRemoved(positionToDelete);
//            }
//        });

        // tworzymy i zwracamy obiekt ViewHolder
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
        // uzupełniamy layout artykułu
        CarPlace article = mArticles.get(i);
        ((MyViewHolder) viewHolder).mPlaceNumber.setText(article.getPlaceNumber());
        ((MyViewHolder) viewHolder).mStatus.setText(article.getStatus());
        ((MyViewHolder) viewHolder).itemView.setBackgroundColor(Color.parseColor(article.getColor()));

    }

    @Override
    public int getItemCount() {
        return mArticles.size();
    }
    // convenience method for getting data at click position
    CarPlace getItem(int id) {
        return mArticles.get(id);
    }
}
