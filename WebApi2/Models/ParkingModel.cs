﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class ParkingModel
    {
        public int ID { get; set; }
        public string PlaceNumber { get; set; }
        public string PlaceStatus { get; set; }
        public string Color { get; set; }
    }
}