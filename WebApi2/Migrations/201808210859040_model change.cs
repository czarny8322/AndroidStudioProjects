namespace WebApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelchange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParkingModels", "PlaceNumber", c => c.String());
            AddColumn("dbo.ParkingModels", "PlaceStatus", c => c.String());
            AddColumn("dbo.ParkingModels", "Color", c => c.String());
            DropColumn("dbo.ParkingModels", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ParkingModels", "Name", c => c.String());
            DropColumn("dbo.ParkingModels", "Color");
            DropColumn("dbo.ParkingModels", "PlaceStatus");
            DropColumn("dbo.ParkingModels", "PlaceNumber");
        }
    }
}
