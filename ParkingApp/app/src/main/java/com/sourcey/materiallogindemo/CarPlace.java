package com.sourcey.materiallogindemo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "carPlaces")
public class CarPlace {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "placeNumber")
    private String mPlaceNumber;

    @ColumnInfo(name = "status")
    private String mStatus;

    @ColumnInfo(name = "color")
    private String mColor;

    public CarPlace(){

    }

    public CarPlace(int uid, String mPlaceNumber, String mStatus, String mColor){
        this.uid = uid;
        this.mPlaceNumber = mPlaceNumber;
        this.mStatus = mStatus;
        this.mColor = mColor;
    }



    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPlaceNumber() {
        return mPlaceNumber;
    }

    public void setPlaceNumber(String mPlaceNumber) {
        this.mPlaceNumber = mPlaceNumber;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String mColor) {
        this.mColor = mColor;
    }

}
