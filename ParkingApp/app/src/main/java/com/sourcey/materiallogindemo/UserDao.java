package com.sourcey.materiallogindemo;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM carPlaces")
    List<CarPlace> getAll();

    @Query("SELECT * FROM carPlaces where placeNumber LIKE  :placeNumber")
    CarPlace findByName(String placeNumber);

    @Query("SELECT * FROM carPlaces where uid LIKE  :id")
    CarPlace findById(int id);

    @Query("SELECT COUNT(*) from carPlaces")
    int countUsers();

    @Insert
    void insertAll(CarPlace... carPlaces);

    @Update
    public void updateCarPlace(CarPlace... carPlaces);

    @Delete
    void delete(CarPlace carPlace);


}
