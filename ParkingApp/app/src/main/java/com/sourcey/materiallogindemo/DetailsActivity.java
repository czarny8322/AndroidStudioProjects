package com.sourcey.materiallogindemo;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sourcey.materiallogindemo.Retrofit.ApiModel;
import com.sourcey.materiallogindemo.Retrofit.RetrofitClientInstance;
import com.sourcey.materiallogindemo.Retrofit.GetDataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailsActivity extends AppCompatActivity {

    ///
    // tag, który jest wykorzystany do logowania
    private static final String CLASS_TAG = "MainActivity";



    AppDatabase db = AppDatabase.getAppDatabase(this);

    int ID;
    String status;
    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        String Uid = intent.getStringExtra("Uid");
        ID = Integer.parseInt(Uid);
        CarPlace carPlace = db.userDao().findById(ID);
        status = carPlace.getStatus();

        TextView textView = findViewById(R.id.place_number);
        TextView textView_status = findViewById(R.id.place_status);

        textView.setText(Uid);
        textView_status.setText(status);

        // ustawiamy przycisk GET
        findViewById(R.id.button_get).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Create handle for the RetrofitInstance interface*/
                //GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<ApiModel>> listCall = service.getAll();
                listCall.enqueue(new Callback<List<ApiModel>>() {
                    @Override
                    public void onResponse(Call<List<ApiModel>> call, Response<List<ApiModel>> response) {
                        showApiLogs(response);
                    }

                    @Override
                    public void onFailure(Call<List<ApiModel>> call, Throwable t) {
                        Log.e(CLASS_TAG, "Unable to submit post to API.");
                    }
                });
            }
        });

        // ustawiamy przycisk POST
        findViewById(R.id.button_post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                service.saveItem("22111111","ffffff","ffffff").enqueue(new Callback<List<ApiModel>>() {
                    @Override
                    public void onResponse(Call<List<ApiModel>> call, Response<List<ApiModel>> response) {
                        if(response.isSuccessful()) {
                            showApiLogs(response);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ApiModel>> call, Throwable t) {
                        Log.e(CLASS_TAG, "Unable to submit post to API.");
                    }
                });
            }
        });

    }

    public void showApiLogs(Response<List<ApiModel>> response){

        List<ApiModel> examples = response.body();
        for (int i = 0; i < examples.size(); i++) {
            Log.e(CLASS_TAG, "examples list : " + examples.get(i).getID() + ", " +
                    examples.get(i).getPlaceStatus() + ", " +
                    examples.get(i).getPlaceNumber() + ", " +
                    examples.get(i).getColor()
            );
        }
    }

    public void book_place(int uid){
        CarPlace currentPlace = db.userDao().findById(uid);
        currentPlace.setStatus("zajęte");
        currentPlace.setColor("#F67777");
        db.userDao().updateCarPlace(currentPlace);
    }

    public void leave_place(int uid){
        CarPlace currentPlace = db.userDao().findById(uid);
        currentPlace.setStatus("wolne");
        currentPlace.setColor("#00FF00");
        db.userDao().updateCarPlace(currentPlace);
    }

    public void onButtonClick_book(View view){
        book_place(ID);
        finish();
    }

    public void onButtonClick_leave(View view){
        leave_place(ID);
        finish();
    }

}
