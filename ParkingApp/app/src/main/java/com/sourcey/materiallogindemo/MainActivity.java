package com.sourcey.materiallogindemo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.materiallogindemo.Retrofit.ApiModel;
import com.sourcey.materiallogindemo.Retrofit.GetDataService;
import com.sourcey.materiallogindemo.Retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// implements MyAdapter.ItemClickListener
public class MainActivity extends AppCompatActivity{

    private final int REQUEST_CODE = 1;
    //public static final String EXTRA_MESSAGE = "com.sourcey.materiallogindemo.MESSAGE";
    AppDatabase db;
    List<CarPlace> carPlaces;
    MyAdapter adapter;

    private static final String CLASS_TAG = "MainActivity";

    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        db = AppDatabase.getAppDatabase(this);

        if (db.userDao().countUsers() == 0){
            populateWithTestData(db);
        }

        carPlaces = db.userDao().getAll();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.articles);
        // w celach optymalizacji
        recyclerView.setHasFixedSize(true);

        // ustawiamy LayoutManagera
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        // ustawiamy animatora, który odpowiada za animację dodania/usunięcia elementów listy
        recyclerView.setItemAnimator(new DefaultItemAnimator());




        // tworzymy adapter oraz łączymy go z RecyclerView

        adapter = new MyAdapter(carPlaces, recyclerView);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //ParkingDataModel movie = myModel.get(position);
                //Toast.makeText(getApplicationContext(), movie.getPlaceNumber() + " is selected!", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(view.getContext(), DetailsActivity.class);
                int _placeNumber = adapter.getItem(position).getUid();
                intent.putExtra("Uid", Integer.toString(_placeNumber));
                startActivityForResult(intent, REQUEST_CODE);
            }

            @Override
            public void onLongClick(View view, int position) {
                //ParkingDataModel movie = myModel.get(position);
                //Toast.makeText(getApplicationContext(), movie.getPlaceNumber() + " is LOOONG selected!", Toast.LENGTH_SHORT).show();
            }
        }));


        SharedPreferences sp1=this.getSharedPreferences("Login", MODE_PRIVATE);
        String Unm = sp1.getString("Unm", null);
        //String pass = sp1.getString("Psw", null);
        TextView textView = (TextView) findViewById(R.id.hello_worldId);
        textView.setText(Unm);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        getDataFromServer();
        //loadNewList(carPlaces);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        // sprawdzamy czy przyszło odpowiednie żądanie
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            // sprawdzamy czy intencja przyniosła odpowiednią wiadomość
            if (intent.hasExtra("result")) {
//                // tutaj uzyskujemy obiekt o tym samym id
//                // ale dotyczy pliku activity_main.xml
//                TextView textView = (TextView) findViewById(R.id.hello_worldId);
//                textView.setText( "fsdfsdf" );
//
//
//                String placeNumber = intent.getStringExtra("placeNumber");
//                String placeStatus = intent.getStringExtra("placeStatus");
//                String placeColor = intent.getStringExtra("placeColor");

                Toast.makeText(getApplicationContext(), "This is my Toast message!",
                        Toast.LENGTH_LONG).show();

                //myModel.get(3).setPlaceNumber(placeColor);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                //newGame();
                return true;
            case R.id.action_logOut:
                logOff();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void logOff() {
        SharedPreferences sp=getSharedPreferences("Login", MODE_PRIVATE);
        SharedPreferences.Editor Ed=sp.edit();
        Ed.putString("Unm",null );
        //Ed.putString("Psw",password);
        Ed.commit();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    private static CarPlace addUser(final AppDatabase db, CarPlace carPlace) {
        db.userDao().insertAll(carPlace);
        return carPlace;
    }

    private static void populateWithTestData(AppDatabase db) {


        db.userDao().insertAll(new CarPlace(55, "55", "zajęte", "#0000ff"),
                                new CarPlace(66, "66", "zajęte", "#0000ff"),
                                new CarPlace(77, "77", "wolne", "#0000ff"),
                                new CarPlace(43, "43", "wolne", "#0000ff"),
                                new CarPlace(34, "34", "zajęte", "#0000ff"),
                                new CarPlace(32, "32", "wolne", "#0000ff"));
    }

    public List<CarPlace> getDataFromServer(){

        final List<CarPlace> dataFromServer = null;

        Call<List<ApiModel>> listCall = service.getAll();
        listCall.enqueue(new Callback<List<ApiModel>>() {
            @Override
            public void onResponse(Call<List<ApiModel>> call, Response<List<ApiModel>> response) {
                //showApiLogs(response);
                loadNewList(response);
            }

            @Override
            public void onFailure(Call<List<ApiModel>> call, Throwable t) {
                //Log.e(CLASS_TAG, "Unable to submit post to API.");
            }
        });

        return dataFromServer;
    }

    public void showApiLogs(Response<List<ApiModel>> response){
        List<ApiModel> examples = response.body();
        for (int i = 0; i < examples.size(); i++) {
            Log.e(CLASS_TAG, "examples list : " + examples.get(i).getID() + ", " +
                    examples.get(i).getPlaceStatus() + ", " +
                    examples.get(i).getPlaceNumber() + ", " +
                    examples.get(i).getColor()
            );
        }
    }


    private void loadNewList(Response<List<ApiModel>> response){
        List<CarPlace> carlist = new ArrayList<>();
        List<ApiModel> apiItem = response.body();

        for(int i=0;i<apiItem.size(); i++ ){

            int id = apiItem.get(i).getID();
            String placeNumber = apiItem.get(i).getPlaceNumber();
            String status = apiItem.get(i).getPlaceStatus();
            String color = apiItem.get(i).getColor();


            db.userDao().insertAll(new CarPlace(id, placeNumber, status, color));


            //carlist.add(singleItem);
        }

        carPlaces.clear();

        //List<CarPlace> new_carPlaces = carlist;
        List<CarPlace> new_carPlaces = db.userDao().getAll();


        Log.e(CLASS_TAG, new_carPlaces.get(2).getStatus());
        carPlaces.addAll(new_carPlaces);

        adapter.notifyDataSetChanged();
    }



//    private void loadNewList(List<CarPlace> carPlaces){
//
//        getDataFromServer();
//
//        carPlaces.clear();
//        List<CarPlace> new_carPlaces = db.userDao().getAll();
//        //List<CarPlace> new_carPlaces = getDataFromServer();
//        carPlaces.addAll(new_carPlaces);
//
//        adapter.notifyDataSetChanged();
//    }
}
